#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
"""Convenience wrapper for running bf_template_match directly from source tree."""
 
from convert_ometiff.convert_ometiff import main
 
if __name__ == '__main__':
    main()